Solo project to catalog and list domain names that people may want to block.  

Current focus on corporations, for which there are no other maintained lists.

Files in this project list the domain names of servers, one per line that can be added to your local hosts file to tell your computer to never talk to servers on that domain name or added to your [pi-hole](https://github.com/pi-hole/pi-hole) blocklists for network wide blocking.


## FAQ

* Your hosts file Location:
  * Linux, Unix and Mac OS X  -> `/etc/hosts`
  * Windows XP, Vista and Windows 7 ->  `C:\WINDOWS\system32\drivers\etc\hosts`
  * Windows 2000  -> `C:\WINNT\system32\drivers\etc\hosts`
  * Windows 98/ME ->  `C:\WINDOWS\hosts`

* Recommended software for editing hosts file
  * HostsMan for Windows
  * tblock for Linux

* More lists
  * https://filterlists.com/
  * https://firebog.net/
  * For game trackers: https://hosts.gameindustry.eu/